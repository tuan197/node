var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/chi-tiet/*.:id', function(req, res, next) {
    var id = req.params.id;
    if (req.session.daxem == null) {
        req.session.daxem = [];
    }
    if (req.session.daxem.indexOf(id) == -1) {
        req.session.daxem.push(id);
    }
    res.render('chi-tiet', { id: id });
});

/* GET home page. */
router.get('/ds', function(req, res, next) {
    res.render('ds', { danhsach: req.session.daxem });
});
module.exports = router;